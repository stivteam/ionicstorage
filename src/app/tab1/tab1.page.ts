import { Component } from '@angular/core';
import { StorageService } from '../storage.service';
import { NumberHolder } from '../data/data';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public nh:NumberHolder[] = new Array();

  constructor(private storageService:StorageService) 
  {}

  public getString()
  {
    this.storageService.get('appStore').then(result => {
      if (result != null) {
      alert(result);
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
  }

  public saveString()
  {
    let str = prompt("Put in some text");
    this.storageService.set("appStore", str);
  }


  public saveArray()
  {
    let nums:Number[] = [0,1,2,3,4,5,6];
    this.storageService.setObject("appArrayStore", nums);
  }

  public getArray()
  {
    this.storageService.get('appArrayStore').then(result => {
      if (result != null) {
      alert(JSON.stringify(result));
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
  }

  public saveMap()
  {
    let myMap = new Map();
     myMap.set('prop1', 123);
     myMap.set('prop1', 456);
     myMap.set('prop2', 'abc');
     myMap.set('prop2', 'def');
     myMap.set('prop3', 'Hello Map');
     this.storageService.setMap("myMap", myMap);
  }

  public getMap()
  {
    this.storageService.getMap('myMap').then(result => {
      if (result != null) {
      alert(JSON.stringify(result,this.replacer));
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
  }

  private replacer(key, value) {
    if(value instanceof Map) {
      return {
        dataType: 'Map',
        value: Array.from(value.entries()), // or with spread: value: [...value]
      };
    } else {
      return value;
    }
  }

    public addClassObject()
    {
      let numHol:NumberHolder = new NumberHolder();
      this.nh.push(numHol);
    }

    public incNum(i:number)
    {
      this.nh[i].increment();
    }

    public saveAllClassObjects()
    {
      this.storageService.setObject("classObjArrayStore", this.nh);
      alert(JSON.stringify(this.nh)); 
    }

    public getAllClassObjects()
    {
     
        this.storageService.get('classObjArrayStore').then(result => {
          if (result != null) {
          alert(JSON.stringify(result));
          result.forEach(res =>{
          let tmpNH:NumberHolder = Object.assign(new NumberHolder(),res) ;
          this.nh.push(tmpNH);
          });
          
          }
        }).catch(e => {
          console.log('error: '+ e);
          // Handle errors here
          });
    }
 





}
